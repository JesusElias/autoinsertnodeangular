import express, { Application } from 'express'
import morgan from 'morgan'
import path from 'path'

import swaggerUi from 'swagger-ui-express'
import * as swaggerDocument from './swagger.json'

import nodejsRoutes from './routes/nodejs'

var cors = require('cors')

const app: Application = express();

// setting
app.use(cors())


app.set('port', 4000);

//middlewares
app.use(morgan('dev'));
app.use(express.json());

//routes

app.use('/api/node', nodejsRoutes)


// this folder for this application will be used to store file
app.use('/uploads', express.static(path.resolve('uploads')));

app.use('/swagger', swaggerUi.serve, swaggerUi.setup(swaggerDocument))

export default app;