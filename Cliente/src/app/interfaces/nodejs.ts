export interface Nodejs {
    _id?: number;
    title:string;
    story_url:string;
    story_title:string;
    author: string;
    created_at:Date;     
  }