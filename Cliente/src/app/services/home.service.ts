import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';

const apiUrl = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  constructor(public http: HttpClient) { }

  
  getAllHome(){

  	return this.http.get(`${apiUrl}/nodejs`);
  	
  }

  deleteHome(id: any): Observable<any> {

  	return this.http.delete(`${apiUrl}/nodejs/${id}`, {responseType: 'text'});
  	
  }
}
