"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateProducto = exports.deleteProducto = exports.getProducto = exports.saveProducto = void 0;
const Producto_1 = __importDefault(require("../models/Producto"));
const saveProducto = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //saving a new producto
    const producto = new Producto_1.default({
        name: req.body.name,
        description: req.body.description
    });
    yield producto.save();
    res.json(producto);
});
exports.saveProducto = saveProducto;
const getProducto = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const producto = yield Producto_1.default.find({}).lean();
    if (!producto)
        return res.status(404).json('No User found');
    res.json(producto);
});
exports.getProducto = getProducto;
const deleteProducto = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    console.log(req.params);
    res.send('deleting');
    const { id } = req.params;
    const producto = yield Producto_1.default.findByIdAndRemove(id);
    console.log(producto);
    res.json(producto);
    console.log(producto);
});
exports.deleteProducto = deleteProducto;
function updateProducto(req, res) {
    return __awaiter(this, void 0, void 0, function* () {
        const { id } = req.params;
        const { name, description } = req.body;
        const updatePhoto = yield Producto_1.default.findByIdAndUpdate(id, {
            name,
            description
        }, { new: true });
        return res.json({
            message: 'Sucessfully Updated',
            updatePhoto
        });
    });
}
exports.updateProducto = updateProducto;
// export async function deletePhoto(req: Request, res: Response): Promise<Response>{
//     const { id } = req.params;
//     const photo = await Photo.findByIdAndRemove(id)
//     if(photo) {
//         fs.unlink(path.resolve(photo.imagePath))
//     }
//     return res.json({
//         message: 'Photo Deleted',
//         photo
//     })
// }
//# sourceMappingURL=producto.Controller.js.map