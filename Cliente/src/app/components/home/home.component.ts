import { Component, OnInit } from '@angular/core';
import { HomeService } from '../../services/home.service';
import { Nodejs } from '../../interfaces/nodejs';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  node: Nodejs[] = [];
  conditionTitle: any;
 
  constructor(public homeService: HomeService) {
  
  }

  ngOnInit(): void {
    this.getNodejs();
  }

  getNodejs(){
    
    this.homeService.getAllHome()
      .subscribe( (resp:any) => {
        this.node = resp;
        console.log(resp);
        console.log(this.node);
        this.node.sort((a, b) => {
          const date1 = new Date(a.created_at);
          const date2 = new Date(b.created_at);
          if (date1 > date2) {return -1;}
          if (date1 < date2) {return 1;}
          return 0;

  
        });
      })
    
  }

  deleteNodejs(id:any){
    this.homeService.deleteHome(id).subscribe(data =>{
      console.log('prueba')
      this.getNodejs();

    }), (error: any) => {
      console.log(error)
    }
  }
  
}

