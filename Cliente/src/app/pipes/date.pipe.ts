import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'date'
})
export class DatePipe implements PipeTransform {

  transform(value: Date): String {
    var monthNamesShort = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var dateToday = new Date();
    var dateValue = new Date(value);
    var conditionYear = dateValue.getFullYear() === dateValue.getFullYear();
    var conditionMonth = dateValue.getMonth() === dateValue.getMonth();
    var conditionDay = dateValue.getDate() === dateToday.getDate();
    var conditionDayDiff = dateValue.getDate() === dateToday.getDate() -1;
    if (conditionYear && conditionMonth && conditionDay) {
      var hours = dateValue.getHours(),
      am;
      var minutes = dateValue.getMinutes();
      hours = (hours + 24 - 2) % 24;
      am = hours < 12 ? 'am' : 'pm';
      hours = (hours % 12) || 12;
      return `${hours}:${minutes} ${am}`;
    };
    if (conditionYear && conditionMonth && conditionDayDiff) {
      return 'Yesterday';
    }
    return `${monthNamesShort[dateValue.getMonth()]} ${dateValue.getDate()}`;
  }

}
