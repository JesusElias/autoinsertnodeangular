"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const photoSchema = new mongoose_1.Schema({
    title: {
        type: String,
        required: true,
        min: 4,
        lowercase: true
    },
    description: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    },
    imagePath: {
        type: String,
        unique: true,
        required: true,
        lowercase: true
    }
});
exports.default = mongoose_1.model('Photo', photoSchema);
//# sourceMappingURL=Photo.js.map