"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteNodejs = exports.getNodejs = exports.saveNodejs = exports.autoPost = exports.getApiPublic = void 0;
const Nodejs_1 = __importDefault(require("../models/Nodejs"));
const cron = require('node-cron');
var rp = require('request-promise');
const getApiPublic = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    var API = process.env.API_PUBLIC;
    console.log(API);
    const nodejs = yield Nodejs_1.default.find({}).lean();
    if (!nodejs)
        return res.status(404).json('No User found');
    res.json(nodejs);
});
exports.getApiPublic = getApiPublic;
function autoPost() {
    cron.schedule("0 * * * *", () => {
        const defaultRequest = rp.defaults({
            baseUrl: process.env.API_PUBLIC,
            json: true,
        });
        const URL = process.env.API_LOCAL + 'api/node/nodejs';
        defaultRequest({
            url: '/search_by_date?query=nodejs',
            method: 'GET',
            json: true
        }).then((body) => {
            const hits = body.hits;
            hits.forEach((hit) => {
                const payload = {
                    story_title: hit.story_title,
                    story_url: hit.story_url,
                    title: hit.title,
                    author: hit.author,
                    created_at: hit.created_at
                };
                console.log(payload);
                rp({
                    url: URL,
                    method: 'POST',
                    body: payload,
                    json: true
                }).then((body) => console.log(body))
                    .catch((err) => {
                    console.log(err.name, err.statusCode);
                });
            })
                .catch((err) => {
                console.log(err.name, err.statusCode);
            });
        });
    });
}
exports.autoPost = autoPost;
const saveNodejs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    //saving 
    const nodejs = new Nodejs_1.default({
        story_title: req.body.story_title,
        story_url: req.body.story_url,
        title: req.body.title,
        author: req.body.author,
        created_at: req.body.created_at
    });
    yield nodejs.save();
    res.json(nodejs);
});
exports.saveNodejs = saveNodejs;
const getNodejs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const nodejs = yield Nodejs_1.default.find({}).lean();
    if (!nodejs)
        return res.status(404).json('No User found');
    res.json(nodejs);
});
exports.getNodejs = getNodejs;
const deleteNodejs = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.send('deleting');
    const { id } = req.params;
    yield Nodejs_1.default.findByIdAndRemove(id);
});
exports.deleteNodejs = deleteNodejs;
//# sourceMappingURL=nodejs.Controller.js.map