"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const nodejsSchema = new mongoose_1.Schema({
    story_title: { type: String },
    story_url: { type: String },
    title: { type: String },
    author: { type: String },
    created_at: { type: Date },
});
exports.default = mongoose_1.model('Nodejs', nodejsSchema);
//# sourceMappingURL=Nodejs.js.map