"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const router = express_1.Router();
const nodejs_Controller_1 = require("../controllers/nodejs.Controller");
router.route('/nodejs')
    .get(nodejs_Controller_1.getApiPublic)
    .get(nodejs_Controller_1.getNodejs)
    .post(nodejs_Controller_1.saveNodejs);
// router.post('/saveNodejs', saveNodejs)
// router.get('/getNodejs', getNodejs)
router.route('/nodejs/:id')
    .delete(nodejs_Controller_1.deleteNodejs);
exports.default = router;
//# sourceMappingURL=nodejs.js.map