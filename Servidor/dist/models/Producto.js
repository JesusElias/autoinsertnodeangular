"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const productoSchema = new mongoose_1.Schema({
    name: { type: String },
    description: { type: String },
});
exports.default = mongoose_1.model('Producto', productoSchema);
//# sourceMappingURL=Producto.js.map